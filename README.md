# Flipp3r #

> A flipper/pinball simulator. Tilting is part of the game!

Current status: playable tech demo. I am open for **gameplay ideas** and **merge requests**.

## Screenshots ##

![Screenshot 1](docs/img/flipp3r_screen_01.png)

# Quick Howtos #

## General ##

- For most Python commands, start the venv first: `. ../.venv/bin/activate`

## Simulator ##

Needs the `flow3r-firmware` sources.  And a symlink to this repo in `python_payload/apps/`.
- `cd ../flow3r-firmware.git; ./sim/run.py Flipp3r`

## Run unit tests ##

`pytest` must be installed before.
- `make test`

## Update apps ##

- Must have access to `/dev/ttyACM0` (replace ID if necessary)
- `make live-test`

## Flash firmware ##

- [Download firmware](https://git.flow3r.garden/flow3r/flow3r-firmware/-/releases), extract it
- Select SD mode on Flow3r
- Copy `flow3r.bin` to the mounted flow3r SD card
