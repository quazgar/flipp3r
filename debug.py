import os

try:
    debug = bool(os.environ.get("DEBUG"))
except AttributeError:
    # debug = True
    debug = False

if debug:
    print("DEBUG = True")
