import os
import math
import sys

try:
    from enum import Enum
    from typing import Optional
except (ImportError, ):
    Enum = object  # NOQA

import leds

from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from st3m.utils import tau
from ctx import Context

try:  # For the simulator only
    from typing import Optional
    sys.path.append(os.path.dirname(__file__))
except ImportError:  # For the flow3r
    sys.path.append("/flash/sys/apps/flipp3r")

import objects  # NOQA
from _constants import *  # NOQA
from debug import debug
from geom2d import (polar_angle, length, dot_product, plus, minus, mult,
                    rotate_around_zero,
                    )  # NOQA


class GameOverScreen(BaseView):
    def __init__(self) -> None:
        super().__init__()
        self._phase = 0.
        self._scale = 1.
        self._angle = 0.

    def draw(self, ctx: Context) -> None:
        # Paint the background with a single color
        ctx.rgb(.86, .12, .12 / 256.)
        ctx.rectangle(-120, -120, 240, 240).fill()

        # Game over text
        ctx.move_to(0, -40)
        ctx.rgb(0, 0, 0)
        ctx.text_align = ctx.CENTER
        ctx.save()
        ctx.font = "Camp Font 2"
        ctx.font_size = 80
        ctx.scale(self._scale, 1)
        ctx.rotate(self._angle)
        ctx.text("Game\nover")
        ctx.restore()
        # ctx.font = "Camp Font 3"
        ctx.font_size = 25
        ctx.move_to(0, 70)
        ctx.text("Press button")

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        self._phase += delta_ms / 1000
        self._scale = math.cos(self._phase)

        iy = ins.imu.acc[0] * delta_ms / 10.0
        ix = ins.imu.acc[1] * delta_ms / 10.0
        ang = math.atan2(ix, iy)
        d_ang = self._angle + (ang + math.pi / 8 * math.sin(self._phase))
        self._angle -= d_ang / 40

        # self._led += delta_ms / 45
        # if self._led >= 40:
        #     self._led = 0

        if self.input.buttons.app.middle.pressed:
            self.vm.pop()


class Flipp3rView(BaseView):
    class AppState(Enum):
        PLAYING = 0
        GAME_OVER = 1
        TERMINATED = 2

    def __init__(self) -> None:
        self._state = self.AppState.PLAYING

        # Ball physics
        self.ball = objects.Ball()
        self.objects: list[objects.EnvironmentObject] = []

        # fancy stuff
        leds.set_slew_rate(255)
        self._bounce_lights: dict[int, list[float]] = {}
        # self._recently_lit = []

        # Game
        self.counter = objects.GamePointCounter(35, (0, 0))

        self._init_environment()

    def _init_environment(self) -> None:
        """Initialize the environment objects."""
        self.objects.append(objects.Boundary(SCREEN_R))
        for idx in range(5):
            self.objects.append(objects.BouncyCircle(
                30,
                rotate_around_zero(*(0, -SCREEN_R), idx * tau / 5),
                touch_idx=idx * 2))
        self.objects.append(self.counter)

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(*BG_RGB).rectangle(-120, -120, 240, 240).fill()

        # Paint game points
        ctx.move_to(-60, -60).rgb(0.9, 0.9, 0.9)
        ctx.text(f"Pts: {int(round(self.counter.count))} ({int(round(self.counter.max_count))})")

        # Draw the environment
        for obj in self.objects:
            obj.draw(ctx)

        # Ball
        ctx.radial_gradient(*self.ball.pos, BALL_R / 2, *self.ball.pos, BALL_R)
        ctx.add_stop(0, BALL_RGB_1, 1)
        ctx.add_stop(1, BALL_RGB_2, 1)
        ctx.arc(*self.ball.pos, BALL_R, 0, tau, 0).fill()

        # handle bounce LEDs
        to_delete = []
        for led, hsv in self._bounce_lights.items():
            leds.set_hsv(led, *hsv)
            hsv[2] = hsv[2] * (1 - SLEW_FACTOR)
            if hsv[2] < .05:
                leds.set_hsv(led, 0, 0, 0)
                to_delete.append(led)
        for led in to_delete:
            self._bounce_lights.pop(led)
        leds.update()

        # Debugging
        # ctx.move_to(-80, 5)
        # ctx.text(f"{self._vx:.0f} | {self._vy:.0f}")

    def think(self, ins: InputState, delta_ms: int) -> None:
        # super().think(ins, delta_ms)
        if self._state == self.AppState.PLAYING:
            delta_s = delta_ms / 1000.0

            self.ball.think(ins, delta_s, flipp3r=self)

            # Update the environment
            for obj in self.objects:
                obj.think(ins=ins, delta_ms=delta_ms)

            # Check for game over
            if self.counter.count <= 0 and self.counter.max_count > 0:
                self._state = self.AppState.GAME_OVER
        elif self._state == self.AppState.GAME_OVER:
            self._state = self.AppState.TERMINATED

    def is_terminated(self) -> bool:
        return self._state == self.AppState.TERMINATED

    def bounce_lights(self, speed) -> None:
        """Bouncing light effect."""

        angle = polar_angle(*self.ball.pos, zero_is_up=True)
        center_led = math.floor(40 * angle / tau)
        hue = min(speed ** 0.8, 325)
        # print(hue)
        val = min(speed / 100, 1.0)
        width = 4
        for dist in range(width):
            i1, i2 = ((center_led - dist) % 40,
                      (center_led + dist) % 40)
            if dist > 0:
                val *= 0.7
            self._bounce_lights[i1] = [hue, 1.0, val]
            self._bounce_lights[i2] = [hue, 1.0, val]


class Flipp3rApp(Application):
    """A wrapper class for the actual game.

This has a running instance of the game, which may be restarted upon game over.
    """

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._game = Flipp3rView()
        self._game_over = GameOverScreen()

    # def on_enter(self, vm: Optional["ViewManager"]) -> None:
    #     super().__init__(vm)
    #     if sel.vm:
    #         self.vm.push(self._game)

    def draw(self, ctx: Context) -> None:
        self._game.draw(ctx)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)  # Let Application do its thing
        if self._game.is_terminated():
            self._game = Flipp3rView()
            self._game_over = GameOverScreen()
            self.vm.push(self._game_over)
        else:
            if not self._game_over.is_active():
                self._game.think(ins, delta_ms)


if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    import st3m.run
    st3m.run.run_view(Flipp3rApp(ApplicationContext()))
