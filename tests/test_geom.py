import math
import unittest.mock as mock
import os
import sys

from math import pi
parent = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.extend([parent,
                 # os.path.join(parent, "..", "flow3r-firmware.git", "python_payload"),
                 os.path.join(parent, "..", "flow3r-firmware.git", "sim", "fakes"),
                 ])

st3m_utils = mock.MagicMock()
st3m_utils.tau = math.tau
with mock.patch.dict("sys.modules", {
        "st3m.application": mock.MagicMock(),
        "st3m": mock.MagicMock(),
        "st3m.utils": st3m_utils,
}):
    import geom2d


def nearly_equal(v1, v2, abs_delta: float = 1e-10):
    """Test if two vectors are nearly equal."""
    for x1, x2 in zip(v1, v2):
        if abs(x2 - x1) > abs_delta:
            return False
    return True


def test_rotate():
    """Test several rotations."""

    # Rotate the zero vector
    start = (0, 0)
    result_0 = geom2d.rotate_around_zero(*start, 0)
    assert result_0 == start
    result_0 = geom2d.rotate_around_zero(*start, st3m_utils.tau / 8)
    assert result_0 == start

    # Rotate the unit vector
    start = (1, 0)
    result_0 = geom2d.rotate_around_zero(*start, 0)
    assert result_0 == start
    result_0 = geom2d.rotate_around_zero(*start, st3m_utils.tau)
    assert nearly_equal(result_0, start)
    result_90 = geom2d.rotate_around_zero(*start, pi / 2)
    assert nearly_equal(result_90, (0, 1))
    result_180 = geom2d.rotate_around_zero(*start, pi)
    assert nearly_equal(result_180, (-1, 0))
    result_270 = geom2d.rotate_around_zero(*start, pi * 3.0 / 2)
    assert nearly_equal(result_270, (0, -1))
    result_minus_90 = geom2d.rotate_around_zero(*start, -pi / 2)
    assert nearly_equal(result_minus_90, (0, -1))
    result_45 = geom2d.rotate_around_zero(*start, pi / 4)
    assert nearly_equal(result_45, (math.sqrt(.5), math.sqrt(.5)))

    # Rotate scaled unit vectors
    start = (1234.0, 0)
    result_0 = geom2d.rotate_around_zero(*start, 0)
    assert result_0 == start
    result_0 = geom2d.rotate_around_zero(*start, st3m_utils.tau)
    assert nearly_equal(result_0, start)
    result_90 = geom2d.rotate_around_zero(*start, pi / 2)
    assert nearly_equal(result_90, (0, 1234.0))
    result_180 = geom2d.rotate_around_zero(*start, pi)
    assert nearly_equal(result_180, (-1234, 0))
    result_270 = geom2d.rotate_around_zero(*start, pi * 3.0 / 2)
    assert nearly_equal(result_270, (0, -1234.0))
    result_minus_90 = geom2d.rotate_around_zero(*start, -pi / 2)
    assert nearly_equal(result_minus_90, (0, -1234.0))
    result_45 = geom2d.rotate_around_zero(*start, pi / 4)
    assert nearly_equal(result_45, (1234 * math.sqrt(.5), 1234 * math.sqrt(.5)))

    start = (1 / 1234.0, 0)
    result_0 = geom2d.rotate_around_zero(*start, 0)
    assert nearly_equal(result_0, start)
    result_0 = geom2d.rotate_around_zero(*start, st3m_utils.tau)
    assert nearly_equal(result_0, start)
    result_90 = geom2d.rotate_around_zero(*start, pi / 2)
    assert nearly_equal(result_90, (0, 1 / 1234.0))
    result_180 = geom2d.rotate_around_zero(*start, pi)
    assert nearly_equal(result_180, (-1 / 1234, 0))
    result_270 = geom2d.rotate_around_zero(*start, pi * 3.0 / 2)
    assert nearly_equal(result_270, (0, -1 / 1234.0))
    result_minus_90 = geom2d.rotate_around_zero(*start, -pi / 2)
    assert nearly_equal(result_minus_90, (0, -1 / 1234.0))
    result_45 = geom2d.rotate_around_zero(*start, pi / 4)
    assert nearly_equal(result_45, (math.sqrt(.5) / 1234, math.sqrt(.5) / 1234))

    # Rotate rotated unit vectors
    start = (0, 1)
    result_0 = geom2d.rotate_around_zero(*start, 0)
    assert nearly_equal(result_0, start)
    result_90 = geom2d.rotate_around_zero(*start, st3m_utils.tau / 4)
    assert nearly_equal(result_90, (-1, 0))
    result_minus_90 = geom2d.rotate_around_zero(*start, -st3m_utils.tau / 4)
    assert nearly_equal(result_minus_90, (1, 0))

    start = (math.sqrt(.5), math.sqrt(.5))
    result_90 = geom2d.rotate_around_zero(*start, st3m_utils.tau / 4)
    assert nearly_equal(result_90, (-math.sqrt(.5), math.sqrt(.5)))
    result_minus_90 = geom2d.rotate_around_zero(*start, -st3m_utils.tau / 4)
    assert nearly_equal(result_minus_90, (math.sqrt(.5), -math.sqrt(.5)))
