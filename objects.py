"""Objects in the environment to interact with.
"""

# import warnings

try:
    from enum import Enum
    from typing import Optional
except (ImportError, ):
    Enum = object  # NOQA
# from typing import Any

from ctx import Context
from st3m.input import InputState
from st3m.utils import tau

from _constants import *
from geom2d import (length, sq_length, normalize, dot_product, plus, minus, mult)
from debug import debug

# warnings.simplefilter("once", category=DeprecationWarning)


class EnvironmentObject:
    """Abstract base class for objects, defines methods which must be implemented.
    """

    def __init__(self, *args, draw_before: list = None, draw_after: list = None, **kwargs):
        """
Parameters
----------
draw_before, draw_after: list, optional
  A list of function which shall be called at the beginning/end of the drawing phase.  These
  functions are called like this:  ``fun(self, ctx)``
        """
        self._draw_before = []
        self._draw_after = []
        if draw_before:
            self._draw_before.extend(draw_before)
        if draw_after:
            self._draw_after.extend(draw_after)

    def is_close(self, ball: "Ball") -> bool:
        """Cheap test if ``ball`` is "close" to this object.

"Close" may for example mean that it is inside this object's bounding box, but any other cheap
concept may be used as well.  This method is meant for preliminary tests, before more expensive
methods are called.

If this objects can interact with the ball , then ``is_close(ball)`` must return True.

Parameters
----------
ball : Ball
  The ``Ball`` to check against.

Returns
-------
out : bool
  True if ``ball`` is close, False otherwise.

Note
----

The default implementation simply returns ``True``.  This method should be reimplemented however by
inheriting classes to improve the performance."""

        # warnings.warn("Notice: You should reimplement this method.", category=DeprecationWarning)
        return True

    def interact(self, ball: "Ball", delta_ms: float) -> bool:
        """Attempt to interact with the ball.

Parameters
----------
ball: Ball
  The ``Ball`` to interact with.  Position is the one the ball would have without any interaction.

delta_s: float
  The current time step, in seconds.

Returns
-------
out: bool
  True if there was any interaction, False otherwise.
        """
        raise NotImplementedError()

    def draw(self, ctx: Context) -> None:
        """Draw this object.  Does nothing by default.
        """
        for draw_fun in self._draw_before:
            draw_fun(self, ctx)
        self._draw(ctx)
        for draw_fun in self._draw_after:
            draw_fun(self, ctx)

    def _draw(self, ctx: Context) -> None:
        """Draw this object itself.  Does nothing by default.
        """
        pass

    def think(self, ins: InputState, delta_ms: int) -> None:
        """Update this object's state.  Does nothing by default.
        """
        pass


###############################################################################
# Implementations #############################################################
###############################################################################

class Boundary(EnvironmentObject):
    """The game boundary. Circular.

Parameters
----------
radius: float
  The radius of the game boundary.
        """

    def __init__(self, radius: float):
        super().__init__()
        self._radius = radius

    def is_close(self, ball: "Ball") -> bool:
        """Cheap test if ``ball`` is "close" to this object.

Parameters
----------
ball : Ball
  The ``Ball`` to check against.

Returns
-------
out : bool
  True if ``ball`` is close, False otherwise.
        """
        return abs(ball.pos[0]) + abs(ball.pos[1]) + ball.radius >= self._radius

    def interact(self, ball: "Ball", delta_s: float) -> bool:
        """Interact with the ball."""

        if not self.is_close(ball):
            return False

        if sq_length(*ball.pos) < (self._radius - ball.radius) * (self._radius - ball.radius):
            return False

        # Transform to "normal" coordinate system, close to where the ball hit the boundary.
        x_normal: float = length(*ball.pos)
        normal: tuple[float, float] = normalize(*ball.pos)
        # v_normal: float = dot_product(*ball.vel, *normal)

        # print(
        #     f"before:  pos: ({ball.pos[0]:.2f}, {ball.pos[1]:.2f}) / vel: ({ball.vel[0]:.2f}, {ball.vel[1]:.2f})")
        delta_pos = mult(-2 * (x_normal - (self._radius - ball.radius)), *normal)
        ball.pos = plus(*ball.pos, *delta_pos)
        ball.vel = minus(*ball.vel,
                         *mult(2 * dot_product(*ball.vel, *normal), *normal))
        ball.vel = mult(1 - BOUNCE_FRICTION, *ball.vel)
        # print(
        #     f"after:   pos: ({ball.pos[0]:.2f}, {ball.pos[1]:.2f}) / vel: ({ball.vel[0]:.2f}, {ball.vel[1]:.2f})")
        # print()

        return True


class Circle(EnvironmentObject):
    """A circular, bouncy, obstacle.

Parameters
----------

radius: float
  The radius of the circle.

center: tuple[float]
  The x, y coordinates of the center.
    """

    def __init__(self, radius: float, center: tuple[float, float]):
        super().__init__()
        self._radius = radius
        self._center = center
        self.color = (0.2, 0.2, 0.7)

    def is_close(self, ball: "Ball") -> bool:
        """Test if a ball is close.

Parameters
----------
ball: Ball
  The ``Ball`` to check against.


Returns
-------
out : bool
  True if ``ball`` is close, False otherwise.
        """
        return ((abs(ball.pos[0] - self._center[0]) < self._radius + ball.radius)
                or (abs(ball.pos[1] - self._center[1]) < self._radius + ball.radius))

    def interact(self, ball: "Ball", delta_s: float) -> bool:
        """Interact with the ball.

Parameters
----------
ball: Ball
  The ``Ball`` object.

delta_s: float
        """
        if not self.is_close(ball):
            return False

        # print(f"is close: {self._center}       /     {ball.pos}")
        rel_pos = ball.pos[0] - self._center[0], ball.pos[1] - self._center[1]
        r = self._radius + ball.radius
        if sq_length(*rel_pos) > r * r:
            return False

        x_normal: float = length(*rel_pos)
        normal: tuple[float, float] = normalize(*rel_pos)
        delta_pos = mult(-0.2 * (x_normal - (self._radius + ball.radius)), *normal)

        ball.pos = plus(*ball.pos, *delta_pos)
        ball.vel = minus(*ball.vel,
                         *mult(2 * dot_product(*ball.vel, *normal), *normal))
        ball.vel = mult(1 - BOUNCE_FRICTION, *ball.vel)
        # print(f"delta: {delta_pos}\tnew pos: {ball.pos}\t new vel: {ball.vel}")

        return True

    def _draw(self, ctx: Context) -> None:
        ctx.rgb(*self.color).arc(*self._center, self._radius, 0, tau, 0).fill()


class BouncyCircle(Circle):
    """A circle which may actively bounce off the ball.
    """

    class ActiveState(Enum):
        COOLING = 0
        COOLED = 1
        CHARGED = 2

    def __init__(self, *args, cooldown: float = 2.0, capacity=0.75, touch_idx: int, **kwargs):
        super().__init__(*args, **kwargs)
        self._base_color: tuple[float, float, float] = tuple(list(self.color).copy())
        self._active_color = (0.941, 0.059, 0.980)
        self._cooldown = cooldown
        self._capacity = capacity
        self._touch_idx = touch_idx
        self._until_next_state: float = 0
        self.state = self.ActiveState.COOLED
        self._repulse = 1e2 * 3
        self._extend = 10

    def interact(self, ball: "Ball", delta_s: float) -> bool:
        """As ``Circle.interact()``, but sometimes may actively repulse the ball.
        """
        old_radius = self._radius
        if self.state == self.ActiveState.CHARGED:
            self._radius += self._extend
        did_interact = super().interact(ball=ball, delta_s=delta_s)
        if self.state == self.ActiveState.CHARGED:
            self._radius = old_radius
        if did_interact and self.state == self.ActiveState.CHARGED:
            delta_v = mult(self._repulse, *normalize(*minus(*ball.pos, *self._center)))
            ball.vel = plus(*ball.vel, *delta_v)
            ball.pos = plus(*ball.pos, *mult(delta_s, *delta_v))
            self.set_state(self.ActiveState.COOLING)
        return did_interact

    def think(self, ins: InputState, delta_ms: float):
        if self.state == self.ActiveState.COOLING:
            self._until_next_state -= delta_ms / 1000
            if self._until_next_state <= 0:
                self.set_state(self.ActiveState.COOLED)
            else:
                # Penalty if pressing too early
                if ins.captouch.petals[self._touch_idx].pressed:
                    self._until_next_state = min(self._until_next_state + 5. * delta_ms / 1000,
                                                 self._cooldown)
        elif self.state == self.ActiveState.COOLED:
            if ins.captouch.petals[self._touch_idx].pressed:
                self.set_state(self.ActiveState.CHARGED)
        elif self.state == self.ActiveState.CHARGED:
            self._until_next_state -= delta_ms / 1000
            if self._until_next_state <= 0:
                self.set_state(self.ActiveState.COOLING)

    def _draw(self, ctx: Context) -> None:
        """ Update by cooldown notification, if applicable."""
        super()._draw(ctx)
        if self.state == self.ActiveState.COOLING or self.state == self.ActiveState.COOLED:
            radius = self._radius * (1 - self._until_next_state / self._cooldown)
            ctx.rgb(*self._active_color).arc(*self._center, radius, 0, tau, 0).stroke()

    def draw_charged_before(self, ctx: Context):
        """Special effects"""
        special_color = (0.8, 0.3, 0.1)
        ctx.radial_gradient(*self._center, self._radius, *self._center, self._radius + self._extend)
        ctx.add_stop(0, special_color, 1)
        ctx.add_stop(1, special_color, 0)
        ctx.arc(*self._center, self._radius + self._extend, 0, tau, 0).fill()

    def set_state(self, new_state: ActiveState):
        """Set the state to active/inactive."""
        if self.state == new_state:
            return
        self.state = new_state
        if new_state == self.ActiveState.CHARGED:
            self.color = self._active_color
            self._until_next_state = self._capacity
            self._draw_before.append(BouncyCircle.draw_charged_before)
        else:
            try:
                self._draw_before.remove(BouncyCircle.draw_charged_before)
            except ValueError:
                pass  # opportunistic removal
            self.color = self._base_color
            if new_state == self.ActiveState.COOLING:
                self._until_next_state = self._cooldown
            elif new_state == self.ActiveState.COOLED:
                self._until_next_state = 0


class Sensor(Circle):
    """Object without physical interaction."""
    class ActiveState(Enum):
        OUTSIDE = 0
        INSIDE = 1

    def __init__(self, radius: float, center: tuple[float, float]):
        super().__init__(radius, center)
        self._color_outside = (0.3, 0.5, 0.3)
        self._color_inside = (0.3, 0.7, 0.3)
        self._state = self.ActiveState.INSIDE
        self.color = self._color_inside
        self.count = 0

    def interact(self, ball: "Ball", delta_s: float) -> bool:
        if not self.is_close(ball):
            self.set_state(self.ActiveState.OUTSIDE)
            return False

        rel_pos = minus(*ball.pos, *self._center)
        r = self._radius - ball.radius
        if sq_length(*rel_pos) > r * r:
            self.set_state(self.ActiveState.OUTSIDE)
            return False
        self.set_state(self.ActiveState.INSIDE)
        return True

    def set_state(self, new_state: ActiveState):
        if self._state == new_state:
            return
        self._state = new_state
        if new_state == self.ActiveState.OUTSIDE:
            self.color = self._color_outside
        elif new_state == self.ActiveState.INSIDE:
            self.count += 1
            self.color = self._color_inside

    def _draw(self, ctx: Context) -> None:
        super()._draw(ctx)
        lw_old = ctx.line_width
        ctx.line_width = 2
        ctx.rgb(*[0.5 * x for x in self.color]).arc(*self._center, self._radius, 0, tau, 0).stroke()
        ctx.line_width = lw_old


class GamePointCounter(Sensor):
    """Sensor which requires the player to activate ot often enough."""

    def __init__(self, radius: float, center: tuple[float, float]):
        super().__init__(radius, center)
        self.max_count = 0
        self._cooldown = 20.  # Time until the next point is lost
        self._time_to_drop = -1.0
        self._grace = 10  # How many points are free initially until decay starts.
        if debug:
            self._grace = 1
            self._cooldown = 1

    def think(self, ins: InputState, delta_ms: float) -> None:
        if self._state == self.ActiveState.INSIDE:
            self._time_to_drop = -1
            return
        if self._time_to_drop == -1:
            # We must be freshly OUTSIDE.
            if self.max_count >= self._grace:
                self._time_to_drop = self._cooldown
        elif self._time_to_drop > 0:
            self._time_to_drop -= delta_ms / 1000
            if self._time_to_drop <= 0:
                self._time_to_drop = -1
                self.count = max(0, self.count - 1)
        else:
            return

    def _draw(self, ctx: Context) -> None:
        "Draw point loss circle, if necessary."
        super()._draw(ctx)
        if self._time_to_drop > 0:
            radius = self._radius * (self._time_to_drop / self._cooldown)
            lw_old = ctx.line_width
            ctx.line_width = 2
            ctx.rgb(*self._color_inside).arc(*self._center, radius, 0, tau, 0).stroke()
            ctx.line_width = lw_old

    def set_state(self, new_state: Sensor.ActiveState):
        old_state = self._state
        super().set_state(new_state)
        if self._state != old_state:
            if new_state == self.ActiveState.INSIDE:
                self.max_count = max(self.max_count, self.count)
                if self.max_count >= self._grace:
                    self._cooldown *= 1 - (1 / 20)


###############################################################################
# Active objects (not environment) ############################################
###############################################################################

class Ball:
    def __init__(self, pos: Optional[tuple[float, float]] = None,
                 vel: Optional[tuple[float, float]] = None):
        if pos:
            self.pos = pos
        else:
            self.pos = (0., 0.)

        if vel:
            self.vel = vel
        else:
            self.vel = (0., 0.)

        self.radius = BALL_R

    def think(self, ins: InputState, delta_s: float, flipp3r: "Flipp3r"):
        """Updating logic.

Parameters
----------

ins : InputState
  Global InputState.

delta_s : float
  The time since the last step.

flipp3r: Flipp3r
  A Flipp3r object which holds a list of EnvironmentObjects and can for example light up LEDs.
        """
        friction = (1 - FRICTION * delta_s)
        self.vel = (friction * (self.vel[0] + ins.imu.acc[1] * delta_s * G_FACTOR),
                    friction * (self.vel[1] + ins.imu.acc[0] * delta_s * G_FACTOR))
        new_pos = (self.pos[0] + self.vel[0] * delta_s,
                   self.pos[1] + self.vel[1] * delta_s)
        # print(f"acc: {ins.imu.acc}\tnew pos: {new_pos}\tnew vel: {self.vel}")
        self.pos = new_pos  # Move into new direction, make changes later.

        for obj in flipp3r.objects:
            if obj.interact(self, delta_s):
                # print(f"Interacted with {obj}.")
                flipp3r.bounce_lights(length(*self.vel))
                break


###############################################################################
# Backgrounds, help screens, other static stuff ###############################
###############################################################################
