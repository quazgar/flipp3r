"""Geometry utility functions.
"""

try:
    raise ImportError()
    import functools
except ImportError:
    import u_functools as functools
import math
import sys

from st3m.utils import tau

try:
    EPSILON = sys.float_info.epsilon
except AttributeError:
    EPSILON = 1e-16


# Simple R^2 -> R functions ###################################################

@functools.lru_cache
def polar_angle(x: float, y: float, zero_is_up=False) -> float:
    """Calculate the mathematical angle.

Returns
-------

result : float
  Number in [0, tau[, clockwise. Right is 0 == tau, unless if ``zero_is_up`` is True (then up is 0).
    """
    if zero_is_up:
        angle = math.atan2(x, -y)
    else:
        angle = math.atan2(y, x)
    angle = angle % tau
    return angle


@functools.lru_cache
def length(x: float, y: float) -> float:
    return math.sqrt(sq_length(x, y))


@functools.lru_cache
def sq_length(x: float, y: float) -> float:
    return x * x + y * y


# Simple R^2 -> R^2 functions #################################################

@functools.lru_cache
def normalize(x: float, y: float) -> tuple[float, float]:
    """Normalize a vector (make its length == 1).

Returns
-------
out : tuple(float, float)
  The normalized vector components."""
    norm = length(x, y)
    if norm < EPSILON:
        raise ZeroDivisionError(
            f"""Trying to divide by {length}, which is close enough to zero for most practical
            purposes.""")
    return mult(1 / norm, x, y)


@functools.lru_cache
def dot_product(x1: float, y1: float, x2: float, y2: float) -> float:
    """Calculate the dot product (x1, y1) . (x2, y2)

Parameters
----------
x1, y1, x2, y2 : float
  The components of the two vectors

Returns
-------
out : float
  The dot product.
    """
    return (x1 * x2) + (y1 * y2)


def plus(x1: float, y1: float, x2: float, y2: float) -> tuple[float, float]:
    """Calculate the sum (v1 + v2) for two vectors."""
    return (x1 + x2, y1 + y2)


def minus(x1: float, y1: float, x2: float, y2: float) -> tuple[float, float]:
    """Calculate the difference (v1 - v2) for two vectors."""
    return (x1 - x2, y1 - y2)


def mult(a: float, x: float, y: float) -> tuple[float, float]:
    """Calculate the scalar product (a * x, a * y)."""
    return (a * x, a * y)


# More complex R^2 -> R^2 functions ############################################

def rotate_around_zero(x: float, y: float, angle: float) -> tuple[float, float]:
    """Rotate a point around the origin.

Parameters
----------
x, y : float
  The original point's coordinates

angle : float
  The angle, in radians.

Returns
-------
out : tuple[float, float]
  The resulting coordinates.
    """
    argument = polar_angle(x, y) + angle
    radius = length(x, y)
    return (radius * math.cos(argument),
            radius * math.sin(argument))
