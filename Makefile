

live-test:
	mpremote exec 'import os; [os.remove(f"/flash/sys/apps/flipp3r/{f}") for f in os.listdir("/flash/sys/apps/flipp3r/")]' || true
	mpremote fs rm /flash/sys/apps/flipp3r || true
	mpremote mkdir /flash/sys/apps/flipp3r || true
	mpremote cp *.py ":/flash/sys/apps/flipp3r/"
	mpremote cp *.toml ":/flash/sys/apps/flipp3r/"
	mpremote run main.py
.PHONY: live-test

test:
	pytest tests -s
.PHONY: test

