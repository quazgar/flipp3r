# Constants for the application

# Colors
BG_RGB = (0, 0, 0)
BALL_RGB_1 = (0.7, 0.7, .78)
BALL_RGB_2 = (0.7, 0.39, .47)


# Geometry
BALL_R = 8

# Screen geometry
SCREEN_R = 120
# - BALL_R/2
# SCREEN_R2 = SCREEN_R ** 2

# Physics
G_FACTOR = 80
FRICTION = 0.05
BOUNCE_FRICTION = 0.1

# Fancy stuff
SLEW_FACTOR = 0.05
