def lru_cache(fun):
    """Cache the return value of a function.

Not a real lru cache (yet)?

Parameters
----------
fun : function
  The function to be cached

Returns
-------
out : function
  A cached version of the function
    """
    cache = {}
    max_size = 128

    def cached(*args, **kwargs):
        # arg_key = list(args)
        # for key, value in kwargs.items():
        #     arg_key.extend((key, value))
        # args_hash = hash(tuple(arg_key))

        # try:
        #     return cache[args_hash]
        # except KeyError:
        #     pass
        result = fun(*args, **kwargs)
        # if len(cache) >= max_size:
        #     cache.clear()
        # cache[args_hash] = result
        return result
    return cached
